//
//  VibrationCell.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit


class VibrationCell: BaseTableViewCell {

    @IBOutlet weak var viewVibrate: UIView!
    @IBOutlet weak var switchVibrate: UISwitch!
    
    private weak var delegate: VibrationCallBack!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        switchVibrate.addTarget(self, action: #selector(switchIsChanged(mySwitch:)), for: UIControlEvents.valueChanged)

    }
    
    func switchIsChanged(mySwitch: UISwitch) {
        delegate?.onSwitch(isVibrate: switchVibrate.isOn)
    }
    
    func setDelegate(delegate: VibrationCallBack) -> Void {
        self.delegate = delegate
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
