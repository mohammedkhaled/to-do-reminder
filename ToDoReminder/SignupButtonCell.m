//
//  SignupButtonCell.m
//  Makank
//
//  Created by Mohammed Khaled (Sob7y) on 1/16/17.
//  Copyright © 2017 Makank. All rights reserved.
//

#import "SignupButtonCell.h"

@implementation SignupButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)init:(NSString*)NibName
{
    self = [super init];
    if (self) {
        NSArray *nib = [[NSBundle mainBundle]
                        loadNibNamed:NibName
                        owner:self
                        options:nil];
        self = [nib objectAtIndex:0];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
