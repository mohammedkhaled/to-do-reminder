//
//  SignupButtonCell.h
//  Makank
//
//  Created by Mohammed Khaled (Sob7y) on 1/16/17.
//  Copyright © 2017 Makank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupButtonCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIView *view_container;
@property (nonatomic, strong) IBOutlet UIButton *btn_signUp;
@property (nonatomic, strong) IBOutlet UILabel *lbl_terms;
@property (nonatomic, strong) IBOutlet UILabel *lbl_forget_password;

- (id)init:(NSString*)NibName;

@end
