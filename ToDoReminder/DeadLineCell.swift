//
//  DeadLineCell.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit
import SwiftDate

class DeadLineCell: BaseTableViewCell {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblDeadline: UILabel!
    @IBOutlet weak var deadLineDatePicker: UIDatePicker!
    
    private weak var delegate: ReminderDeadLineCallBack?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        deadLineDatePicker.datePickerMode = UIDatePickerMode.dateAndTime
    }
    
    @IBAction func datePickerAction(_ sender: AnyObject) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let strDate = dateFormatter.string(from: deadLineDatePicker.date)
        self.lblDeadline.text = strDate
        
        print("datepicker date = \(GeneralUtil.getDateWithTimeZone(from: deadLineDatePicker.date, locale: Locale.current))")
        print("date today = \(GeneralUtil.getDateWithTimeZone(from: Date(), locale: Locale.current)))")
        
        delegate?.onDeadlineReceived(deadLineDate: GeneralUtil.getDateWithTimeZone(from: deadLineDatePicker.date, locale: Locale.current))
    }
    
    func setDelegate(delegate: ReminderDeadLineCallBack) -> Void {
        self.delegate = delegate
    }
    
}
