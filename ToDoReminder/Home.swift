//
//  FirstViewController.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit
import  UserNotifications

class Home: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    
    var reminderObj: Reminder!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Home View"
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeCell {
            
            reminderObj = CacheUtil.getNotCompletedReminders()[indexPath.row]
            cell.lblReminderBody.text = reminderObj.bodyValue
            
            if reminderObj.getRemainingTime() > 0 {
                cell.lblRemainingTime.text = "\(reminderObj.getRemainingTime()) minutes"
            } else {
                cell.lblRemainingTime.text = "Date Passed"
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Complete" //or customize for each indexPath
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if CacheUtil.getNotCompletedReminders().count > 0 {
            return CacheUtil.getNotCompletedReminders().count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            reminderObj = Reminder()
            reminderObj = CacheUtil.getNotCompletedReminders()[indexPath.row]
            reminderObj.isCompletedValue = true
            CacheUtil.editReminder(reminderObj: reminderObj)
            print("reminder Object after setting = \(reminderObj)")
            
            tableView.reloadData()
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    @IBAction func stopNotification(_ sender: AnyObject) {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        // or you can remove specifical notification:
        // center.removePendingNotificationRequests(withIdentifiers: ["FiveSecond"])
    }


}

