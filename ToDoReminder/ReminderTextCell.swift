//
//  ReminderTextCell.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit


class ReminderTextCell: BaseTableViewCell, UITextFieldDelegate {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var txtReminder: UITextField!
    
    private weak var delegate: ReminderBodyCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.txtReminder.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    func getReminderBodyText() ->String {
        if(validate()){
            return txtReminder.text!
        } else{
            return ""
        }
    }
    
    func validate() -> Bool {
        if(txtReminder.text == ""){
            showError(errorMessage: "Please Enter Empty Fields")
            return false
        }
        return true
    }
    
    @IBAction func textFieldDidChange(_ textField: UITextField) {
        
        delegate?.onBodyReceived(bodyText: textField.text!)
    }
    
    func setDelegate(delegate: ReminderBodyCallBack) -> Void {
        self.delegate = delegate
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
