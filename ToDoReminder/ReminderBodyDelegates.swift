//
//  ReminderBodydelegates.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/2/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import Foundation

@objc public protocol ReminderBodyCallBack:class {
    func onBodyReceived(bodyText: String)
}
