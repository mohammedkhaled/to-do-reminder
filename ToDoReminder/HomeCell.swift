//
//  HomeCell.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/3/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var lblReminderBody: UILabel!
    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var btnComplete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
