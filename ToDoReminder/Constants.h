//
//  Constants.h
//  Community
//
//  Created by khaled el morabea on 7/14/15.
//  Copyright (c) 2015 Ibtikar. All rights reserved.
//

#ifndef Community_Constants_h
#define Community_Constants_h

#define Language @"language"
#define ARABIC @"ar"
#define ENGLISH @"en"


#define FONT_REGULAR @"DINNextLTArabic-Regular"


#define SHOW_STANDARD_ALERT(title, messageText, okButton, cancelButton) UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:messageText delegate:self cancelButtonTitle:cancelButton otherButtonTitles:okButton,nil];[alertView show];




typedef enum {
    USER_NAME = 0,
    USER_PASSWORD = 1,
    USER_PHONE = 2,
    SUBMIT_BUTTON = 3
    
} SIGNUPForm;

typedef enum {
    REQUEST_TIME = 0,
    REQUEST_SPECIALIZE = 1,
    REQUEST_CERTIFICATE = 2,
    REQUEST_PAYMENT = 3,
    REQUEST_NOTES = 4,
    REQUEST_SUBMIT = 5
    
} REQUEST_FORM;

typedef enum {
    REQUEST_DETAILS_INFO = 0,
    REQUEST_DETAILS_TIME = 1,
    REQUEST_DETAILS_LOCATION = 2,
    REQUEST_DETAILS_PAYMENT = 3,
    REQUEST_DETAILS_NOTES = 4,
    REQUEST_DETAILS_SUBMIT = 5
    
} REQUEST_DETAILS_FORM;


//KEYBOARDS
#pragma mark -KeyBoard Sizes

#define KEYBOARD_IPAD 236
#define KEYBOARD_IPHONE_4 224
#define KEYBOARD_IPHONE_5 224
#define KEYBOARD_IPHONE_6 225
#define KEYBOARD_IPHONE_6PLUS 236

//App Colors
#define CL_NAVIGATION_COLOR    [UIColor colorWithRed:243/255.0 green:156/255.0 blue:38/255.0 alpha:1.0]
#define CL_NAVIGATION_TEXT_COLOR    [UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1]
#define CL_APP_GREEN_COLOR    [UIColor colorWithRed:77.0/255.0f green:208.0/255.0f blue:225.0/255.0f alpha:1]
#define CL_APP_GRAY_COLOR    [UIColor colorWithRed:146.0/255.0f green:146.0/255.0f blue:146.0/255.0f alpha:1]
#define CL_SliderNormal [UIColor colorWithRed:217.0/255.0f green:217.0/255.0f blue:217.0/255.0f alpha:1]
#define CL_Davy_Gray [UIColor colorWithRed: 84/255.0 green: 84/255.0 blue: 84/255.0 alpha: 1.0]



#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6PLUS (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)



//KEYS

#define USER_DATA @"USER_DATA"
#define USER_TOKEN @"User_Token"
#define KEY_USER_LOCATION @"userLocation"
#define iOS_TOKEN @"H4pbLctgKt7GbsJLVenknG53jK7Gxsm"
#define KEY_SEND_TOKEN @"send_token"

#define PUSH_PROFILE @"open_profile"
#define PUSH_USER_REQUESTS @"open_user_requests"
#define PUSH_NOTIFICATIONS @"open_notifications"
#define PUSH_USER_WALLET @"open_user_wallet"
#define PUSH_SETTING @"open_setting"
#define PUSH_FIRST_SCREEN @"open_first_screen"

#define LATITUDE @"lat"
#define LONGITUDE @"lon"


#define GoogleMapsApiKey @"AIzaSyD61iipZRH2-ekjIBcuvkCG0O5rgXdp3nQ"//AIzaSyCy_YQc-co_kqWhKSj9feWqJuQkGxPnoAY
#define GooglePlacesApiKey @"AIzaSyD61iipZRH2-ekjIBcuvkCG0O5rgXdp3nQ"

#define LOCATION_ENABLED @"Location enabled"
#define LOCATION_DISABLED @"Location disabled"


//Domain

#define Domain @"http://138.197.44.35/saleem/public/"

//Register Token
#define REGISTER_TOKEN         Domain@"api/auth/device/register"

//Login Flow
#define REGISTER_NEW_USER      Domain@"api/user/register"
#define REGISTER_VERIFY_CODE   Domain@"api/user/verify-phone"
#define LOGIN_USER             Domain@"api/user/login"

//Specialization
#define URL_GET_SPECIALIZATION   Domain@"api/auth/patient/list-doctor-specialization"


//Certificates
#define URL_GET_CERTIFICATES     Domain@"api/auth/patient/list-doctor-certificates"
#define URL_GET_REQUESTS         Domain@"api/auth/patient/list-patient-requests"

#define URL_GET_NEW_REQUESTS             Domain@"api/auth/patient/list-new-requests"
#define URL_GET_INPROGRESS_REQUERSTS     Domain@"api/auth/patient/list-inporgress-requests"
#define URL_GET_CLOSED_REQUESTS          Domain@"api/auth/patient/list-closed-requests"


//Request
#define URL_REQUEST_DOCTOR      Domain@"api/auth/patient/request-doctor"


//Edit Profile
#define URL_EDIT_PROFILE        Domain@"api/auth/user/edit-profile"


//Logout
#define LOGOUT_USER             Domain@"api/auth/user/logout"

// rate doctor
#define RATE_DOCTOR             Domain@"api/auth/patient/rate-request-doctor"

//Google Address Api
#define GET_ADDRESS    @"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f"


#endif
