//
//  General.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/8/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import Foundation
import  UserNotifications


class GeneralUtil {
    
    static func addLocalNotificationWithReminder(reminder: Reminder) -> Void {
        let content = UNMutableNotificationContent()
        //        content.title = NSString.localizedUserNotificationString(forKey: "Elon said:", arguments: nil)
        //        content.body = NSString.localizedUserNotificationString(forKey: "Hello Tom！Get up, let's play with Jerry!", arguments: nil)
        content.title = "You should finish tha task"
        content.body = reminder.bodyValue
        content.sound = UNNotificationSound.init(named: "alarm_tone.mp3")
        // NSNumber类型数据
        content.badge = NSNumber(integerLiteral: UIApplication.shared.applicationIconBadgeNumber + 1);
        content.categoryIdentifier = "com.elonchan.localNotification"
        // Deliver the notification in five seconds.
        /**** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'time interval must be at least 60 if repeating'*/
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: TimeInterval(reminder.remainingValue * 60), repeats: false)
        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
        
        // Schedule the notification.
        let center = UNUserNotificationCenter.current()
        center.add(request)
    }
    
     static func getDateWithTimeZone(from date: Date,locale:Locale) -> Date {
        
        print("date = \(date)")
        let dateString = date.description(with: locale)
        print("date with description= \(dateString)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM d, yyyy 'at' h:mm:ss a zzzz"
        let localeDate = dateFormatter.date(from: dateString)
        
        print("date after converting = \(String(describing: localeDate))")
        
        return localeDate!
        
    }
    
    
}
