//
//  AddReminder.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit
import  UserNotifications


class AddReminder: BaseViewController, UITableViewDelegate, UITableViewDataSource, ReminderDeadLineCallBack, ReminderBodyCallBack, RepeatCallBack, VibrationCallBack {

    @IBOutlet weak var tableView: UITableView!
    
    private var reminderObj = Reminder()
    var reminderText: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onDonePressed))
        navigationItem.rightBarButtonItem = add
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row==0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderTextCell", for: indexPath) as? ReminderTextCell {
                
                cell.setViewController(view: self)
                cell.setDelegate(delegate: self)
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
                
            } else {
                return UITableViewCell()
            }
            
        } else if indexPath.row == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "DeadLineCell", for: indexPath) as? DeadLineCell {
                
                cell.setViewController(view: self)
                cell.setDelegate(delegate: self)
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
                
            } else {
                return UITableViewCell()
            }
            
        } else if indexPath.row == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RepeatCell", for: indexPath) as? RepeatCell {
                
                cell.setViewController(view: self)
                cell.setDelegate(delegate: self)
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
                
            } else {
            
                return UITableViewCell()
            }
            
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "VibrationCell", for: indexPath) as? VibrationCell {
                
                cell.setViewController(view: self)
                cell.setDelegate(delegate: self)
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
                
            } else {
                return UITableViewCell()
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row==1 {
            return 235
        }
        
        return 70
    }
    
    @IBAction func onDonePressed(sender: UIButton) {
        
        let index = IndexPath(row: 0, section: 0)
        let cell: ReminderTextCell = self.tableView.cellForRow(at: index) as! ReminderTextCell
        
        reminderObj.bodyValue = cell.getReminderBodyText()
        reminderObj.id = CacheUtil.getAllReminders().count+1
        
        CacheUtil.addReminder(reminderObj: reminderObj)
        GeneralUtil.addLocalNotificationWithReminder(reminder: reminderObj)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //Cells Delegates
    
    func onBodyReceived(bodyText: String) {
        reminderObj.bodyValue = bodyText
//        print("reminderObj.bodyValue = \(String(describing: reminderObj.bodyValue))")
    }
    
    func onDeadlineReceived(deadLineDate: Date) {
        reminderObj.deadLineValue = deadLineDate
//        print("reminderObj.deadLineValue = \(String(describing: reminderObj.deadLineValue))")
    }
    
    func onSwitch(isVibrate: Bool) {
        reminderObj.isVibrateValue = isVibrate
//        print("reminderObj.isVibrateValue = \(String(describing: reminderObj.isVibrateValue))")
    }
    
    func onRepeatSwitch(isRepeat: Bool) {
        reminderObj.isRepeatValue = isRepeat
//        print("reminderObj.isRepeatValue = \(String(describing: reminderObj.isRepeatValue))")
    }
    
    
}
