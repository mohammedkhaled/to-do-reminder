//
//  Reminder.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

class Reminder: Object {
    
    private dynamic var body = ""
    private dynamic var deadLine = Date(timeIntervalSince1970: 1)
    private var isRepeat = false
    private var isVibrate = false
    var isCompleted = false
    private var remainingTime = 0
    dynamic var id = 0
    
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["bodyValue", "deadLineValue", "isRepeatValue", "isVibrateValue","isCompletedValue"]
    }

    var bodyValue: String {
        set {
            self.body = newValue
        }
        get {
            return body
        }
    }
    
    var deadLineValue: Date {
        set{
            self.deadLine = newValue
        }
        
        get{
            return deadLine
        }
    }
    
    
    var isRepeatValue: Bool {
        set{
            self.isRepeat = newValue
        }
        
        get{
            return isRepeat
        }
    }
    
    var isVibrateValue: Bool {
        set{
            self.isVibrate = newValue
        }
        
        get{
            return isVibrate
        }
    }
    
    var isCompletedValue: Bool {
        set{
            self.isCompleted = newValue
        }
        
        get{
            return isCompleted
        }
    }
    
    var remainingValue: Int {
        set{
            self.remainingTime = newValue
        }
        
        get{
            return getRemainingTime()
        }
    }
    
    func getRemainingTime() -> Int {
        let calendar = NSCalendar.current
        let component = calendar.dateComponents([.minute], from: GeneralUtil.getDateWithTimeZone(from: Date(), locale: Locale.current), to: GeneralUtil.getDateWithTimeZone(from: deadLineValue, locale: Locale.current))
                
        print("compenent = \(component)")
        
        if let hours = component.minute {
            return hours
        }
        
        return 0
    }
    
//    init(body: String, deadLine: Date, isRepeat: Bool, isVibrate: Bool) {
//        self.body = body
//        self.deadLine = deadLine
//        self.isRepeat = isRepeat
//        self.isVibrate = isVibrate
//        
//    }
    
    
}
