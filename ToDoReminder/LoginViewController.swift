//
//  LoginViewController.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/6/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import PopupDialog

class LoginViewController: UIViewController {
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnSignin: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpView()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
//        let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(addPopup), userInfo: nil, repeats: false)
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.viewLogin.frame.origin.y == 0{
                self.viewLogin.frame.origin.y -= keyboardSize.height - 110
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.viewLogin.frame.origin.y != 0{
                self.viewLogin.frame.origin.y += keyboardSize.height + 110
            }
        }
    }
    
    func setUpView() -> Void {
        txtUserName.placeholder = "Email or Mobile No."
        txtUserName.title = "Email or Mobile No."
        
        txtPassword.placeholder = "Password"
        txtPassword.title = "Password"
        
        txtUserName.lineHeight = 0.5 // bottom line height in points
        txtUserName.selectedLineHeight = 1.0
        
        txtPassword.lineHeight = 0.5 // bottom line height in points
        txtPassword.selectedLineHeight = 1.0
        
        viewContainer.layer.cornerRadius = 12.0
        viewContainer.layer.masksToBounds = true
        
        viewRegister.layer.cornerRadius = 12.0
        viewRegister.layer.masksToBounds = true
        
        btnSubmit.layer.cornerRadius = 12.0
        btnSubmit.layer.masksToBounds = true
    }
    
    @IBAction func addPopup(sender: UIButton) {
        self.addPopup()
    }

    @IBAction func changeViews(sender: UIButton) {
        if sender.tag==1 {
            UIView.animate(withDuration: 0.3, animations: {
                self.viewContainer.alpha = 1.0
                self.viewRegister.alpha = 0.0
                
                self.btnSignin.setTitleColor(UIColor.init(colorLiteralRed: 55/255.0, green: 193/255.0, blue: 206/255.0, alpha: 1.0), for: .normal)
                self.btnSignup.setTitleColor(UIColor.lightGray, for: .normal)
                self.btnSubmit.setTitle("Sign in", for: .normal)
                self.btnSubmit.backgroundColor = UIColor.init(colorLiteralRed: 55/255.0, green: 193/255.0, blue: 206/255.0, alpha: 1.0)
            })
            
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.viewContainer.alpha = 0.0
                self.viewRegister.alpha = 1.0
                
                self.btnSignup.setTitleColor(UIColor.init(colorLiteralRed: 55/255.0, green: 193/255.0, blue: 206/255.0, alpha: 1.0), for: .normal)
                self.btnSignin.setTitleColor(UIColor.lightGray, for: .normal)
                
                self.btnSubmit.setTitle("Sign up", for: .normal)
                self.btnSubmit.backgroundColor = UIColor.init(colorLiteralRed: 219/255.0, green: 223/255.0, blue: 228/255.0, alpha: 1.0)
            })
        }
    }
    
    func addPopup() -> Void {
        
        // Prepare the popup assets
        let title = "THIS IS THE DIALOG TITLE"
        let message = "This is the message section of the popup dialog default view"
        let image = UIImage(named: "pexels-photo-103290")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: image, buttonAlignment: .horizontal, transitionStyle: .bounceUp, gestureDismissal: true)
        
        // Create buttons
        let buttonOne = CancelButton(title: "CANCEL") {
            print("You canceled the car dialog.")
        }
        
        let buttonTwo = DefaultButton(title: "ADMIRE CAR") {
            print("What a beauty!")
        }
        
        let buttonThree = DefaultButton(title: "BUY CAR", height: 60) {
            print("Ah, maybe next time :)")
        }
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButton(buttonTwo)
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    /*
    func showCustomDialog(animated: Bool = true) {
        
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "RATE", height: 60) {
            self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
 */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
