//
//  Cache.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/2/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import Foundation
import RealmSwift

class CacheUtil {
    
    private static let cache = try! Realm()
    
    
    static func addReminder(reminderObj: Reminder?) -> Void {
        if let data = reminderObj {
            //Check if the entered 'id' is not already present
            try! self.cache.write {
                //Add object to Realm
                self.cache.add(data)
            }
        }
    }
    
    static func editReminder(reminderObj: Reminder?) -> Void {
        let savedReminder = self.cache.objects(Reminder.self).filter("id = %@",0 ).first
//            //Check if the entered 'id' is not already present
            try! self.cache.write {
                //Add object to Realm
                savedReminder?.isCompleted = true
            }
        
    }
    
    static func getAllReminders() -> [Reminder] {
        return Array (CacheUtil.cache.objects(Reminder.self))
    }
    
    static func getNotCompletedReminders() -> [Reminder] {
        var notCompletedReminders = [Reminder]()
        
        for reminder in getAllReminders() {
            if !reminder.isCompletedValue {
                notCompletedReminders.append(reminder)
            }
        }
        return notCompletedReminders
    }
    
    static func getCompletedReminders() -> [Reminder] {
        var completedReminders = [Reminder]()
        
        for reminder in getAllReminders() {
            if reminder.isCompletedValue {
                completedReminders.append(reminder)
            }
        }
        return completedReminders
    }
}
