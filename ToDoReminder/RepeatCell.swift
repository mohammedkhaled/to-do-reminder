//
//  RepeatCell.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit


class RepeatCell: BaseTableViewCell {

    @IBOutlet weak var viewRepeat: UIView!
    @IBOutlet weak var switchRepeat: UISwitch!
    
    var switchState: Bool!
    
    private weak var delegate: RepeatCallBack!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        switchRepeat.addTarget(self, action: #selector(switchIsChanged(mySwitch:)), for: UIControlEvents.valueChanged)
    }
    
    
    func switchIsChanged(mySwitch: UISwitch) {
        delegate?.onRepeatSwitch(isRepeat: switchRepeat.isOn)
    }
    
    func setDelegate(delegate: RepeatCallBack) -> Void {
        self.delegate = delegate
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
