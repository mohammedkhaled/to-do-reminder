//
//  BaseTableViewCell.swift
//  ToDoReminder
//
//  Created by Mohammed Khaled (Sob7y) on 8/1/17.
//  Copyright © 2017 Ole. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    private var parentView:BaseViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViewController(view:BaseViewController) -> Void {
        self.parentView = view
    }
    
    func showError(errorMessage: String) -> Void {
        parentView.showError(errorMessage: errorMessage)
    }
    
}
